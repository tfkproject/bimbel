<?php
session_start();
if (empty($_SESSION)) {
	header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else{
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
include("head.php");
?>

<body>

    <div id="wrapper">

	<?php include("nav.php"); ?>
		
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Registrasi Bimbel</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Registrasi Data
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form>
									<?php
									$id_siswa = $_GET['id_siswa'];
									$query = "select * from `siswa` where id_siswa = $id_siswa;";
									$eksekusi = mysqli_query($koneksi, $query);
									while($row = mysqli_fetch_array($eksekusi)){
										$id = $row['id_siswa'];
										$parameter = '?id_siswa='.$id;
									?>
										<input name="id_siswa" value="<?php echo $id;?>" class="form-control" placeholder="contoh: Joko" type="hidden">
										<div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" value="<?php echo $row['nama'];?>" class="form-control" placeholder="contoh: Joko" disabled>
                                        </div>
										<div class="form-group">
											<label>Tahun</label>
											<div class="form-group input-group">
												<select name="thn" class="form-control" id="tahun">
													<?php
													if(!empty($_GET['thn'])){
														$tahun = $_GET['thn'];
														?>
														<option value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
														<?php
													}
													$query = "select tahun from `gelombang` group by tahun";
													$eksekusi = mysqli_query($koneksi, $query);
													while($row = mysqli_fetch_array($eksekusi)){
														$tahun = $row['tahun'];
													?>
													<option value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
													<?php
													}
													?>											
												</select>
												<span class="input-group-btn">
													<button id="btn_tahun" name="cr_thn" value="klik" class="btn btn-default" type="submit"><i class="fa fa-search"></i>
													</button>
												</span>
											</div>
										</div>
										
										<?php
										if(isset($_GET['cr_thn'])){
											?>
											<script>
											var slc = document.getElementById("tahun");
											slc.disabled = true;
											var btn = document.getElementById("btn_tahun");
											btn.disabled = true;
											</script>
											<input name="cr_thn" value="klik" class="form-control" placeholder="contoh: Joko" type="hidden">
											<input name="thn" value="<?php echo $_GET['thn'];?>" class="form-control" type="hidden">
											<div class="form-group">
												<label>Gelombang ke</label>
												<div class="form-group input-group">
													<select name="glb" class="form-control" id="gelombang">
														<?php
														if(!empty($_GET['glb'])){
															$gelombang = $_GET['glb'];
															?>
															<option value="<?php echo $gelombang;?>"><?php echo $gelombang;?></option>
															<?php
														}
														$tahun = $_GET['thn'];
														$query = "select gelombang_ke from `gelombang` where tahun = $tahun";
														$eksekusi = mysqli_query($koneksi, $query);
														while($row = mysqli_fetch_array($eksekusi)){
															$gelombang = $row['gelombang_ke'];
														?>
														<option value="<?php echo $gelombang;?>"><?php echo $gelombang;?></option>
														<?php
														}
														?>
													</select>
													<span class="input-group-btn">
														<button id="btn_gelombang" name="cr_glb" value="klik" class="btn btn-default" type="submit"><i class="fa fa-search"></i>
														</button>
													</span>
												</div>
											</div>
											<?php
										}
										if(isset($_GET['cr_glb'])){
											?>
											<input name="cr_glb" value="klik" class="form-control" type="hidden">
											<input name="thn" value="<?php echo $tahun;?>" class="form-control" type="hidden">
												<?php
												$tahun = $_GET['thn'];
												$gelombang = $_GET['glb'];
												$query_ = "select * from `gelombang` where tahun = $tahun and gelombang_ke = $gelombang";
												$eksekusi_ = mysqli_query($koneksi, $query_);
												$row_ = mysqli_fetch_array($eksekusi_)
												?>
												<div class="form-group">
													<label>Biaya Bimbel</label>
													<div class="form-group input-group">
														<span class="input-group-addon">Rp</span>														
														<input type="number" name="by_bim" value="<?php echo $row_['biaya_bimbel'];?>" class="form-control" disabled>
													</div>
												</div>
												<div class="form-group">
													<label>Jumlah cicilan</label>
													<div class="form-group input-group">														
														<input type="number" name="jum_cicil" value="<?php echo $row_['jumlah_cicilan'];?>" class="form-control" disabled>
														<span class="input-group-addon"><b>X</b></span>
													</div>
												</div>
												<a href="add_registrasi_process.php?id_siswa=<?php echo $id;?>&id_gelombang=<?php echo $row_['id_gelombang'];?>"><input type="button" class="btn btn-default" value="Submit"></a>
												<?php
										}
										?>
									<?php
									}
									?>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php
	include("script.php");
	?>

</body>

</html>
<?php
}
?>