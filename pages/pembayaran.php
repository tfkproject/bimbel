<?php
session_start();
if (empty($_SESSION)) {
	header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else{
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
include("head.php");
?>

<body>

    <div id="wrapper">

	<?php include("nav.php"); ?>
	
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Pembayaran Uang Bimbel</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tabel Data Siswa
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						
						<!-- /.table-responsive -->						
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Tahun</th>
                                        <th>Gelombang Ke</th>
                                        <th>Biaya Bimbel</th>
                                        <th>Jumlah Cicilan</th>
                                        <th>Total Bayar</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
									$query = "select * from `registrasi` inner join `siswa` on `registrasi`.`id_siswa` = `siswa`.`id_siswa` inner join `gelombang` on `registrasi`.`id_gelombang` =  `gelombang`.`id_gelombang`";
									$eksekusi = mysqli_query($koneksi, $query);
									while($row = mysqli_fetch_array($eksekusi)){
										$id = $row['id_siswa'];
										$jum_cicil = $row['jumlah_cicilan'];
										
										$id_gelombang = $row['id_gelombang'];
										$parameter = '?id_siswa='.$id.'&jum_cicil='.$jum_cicil.'&id_gelombang='.$id_gelombang;
										
										$query2= "select sum(jumlah) as total from pembayaran where id_siswa = $id";
										$eksekusi2 = mysqli_query($koneksi, $query2);
										$row2 = mysqli_fetch_array($eksekusi2)
								?>
                                    <tr class="gradeU">
										<td><?php echo $row['nama'];?></td>
										<td><?php echo $row['tahun'];?></td>
										<td><?php echo $row['gelombang_ke'];?></td>
										<td class="center">Rp.&nbsp;<?php echo $row['biaya_bimbel'];?></td>
										<td class="center"><?php echo $row['jumlah_cicilan'];?>&nbsp;X</td>
										<td>Rp.&nbsp;<?php echo $row2['total'];?></td>
										<td class="center">
										<?php
										$ket = $row['keterangan'];
										if($ket == 'B'){
											?>
											<font color="red">Belum Lunas</font>
											<?php
										}
										if($ket == 'L'){
											?>
											<font color="green">Lunas</font>
											<?php
										}
										?>
										</td>
										<td class="center">
											<a href="pembayaran_detail.php<?php echo $parameter;?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-folder-open-o fa-fw"></i></button></a>
										</td>
                                    </tr>
                                <?php
									}
								?>
                                    
                                </tbody>
                            </table>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php
	include("script.php");
	?>

</body>

</html>
<?php
}
?>