<?php
session_start();
if (empty($_SESSION)) {
	header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else{
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
include("head.php");
?>

<body>

    <div id="wrapper">

	<?php include("nav.php"); ?>
		
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Data Gelombang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Edit Data
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action="edit_gelombang_process.php" method="POST">
									<?php
									$id_gelombang = $_GET['id_gelombang'];
									$query = "select * from `gelombang` where id_gelombang = $id_gelombang;";
									$eksekusi = mysqli_query($koneksi, $query);
									while($row = mysqli_fetch_array($eksekusi)){
										$id = $row['id_gelombang'];
										$parameter = '?id_gelombang='.$id;
									?>
										<input name="id_gelombang" value="<?php echo $id;?>" class="form-control" type="hidden">
										<div class="form-group">
                                            <label>Tahun</label>
                                            <input type="number" name="thn" value="<?php echo $row['tahun']?>" class="form-control" placeholder="contoh: 2017">
                                        </div>
										<div class="form-group">
                                            <label>Gelombang Ke</label>
                                            <input type="number" name="glbg" value="<?php echo $row['gelombang_ke']?>" class="form-control" placeholder="contoh: 1">
                                        </div>
                                        <div class="form-group">
                                            <label>Biaya Bimbel</label>
											<div class="form-group input-group">
												<span class="input-group-addon">Rp</span>
												<input type="number" name="by_bim" value="<?php echo $row['biaya_bimbel']?>" class="form-control" placeholder="contoh: 1507000">
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah Cicilan</label>
											<div class="form-group input-group col-lg-2">
												<input type="number" name="jum_cil" value="<?php echo $row['jumlah_cicilan']?>" class="form-control" placeholder="contoh: 3">
												<span class="input-group-addon"><b>X</b></span>
											</div>
                                        </div>
									<?php
										}
									?>
                                        <button name="submit" value="submit" type="submit" class="btn btn-default">Update</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php
	include("script.php");
	?>

</body>

</html>
<?php
}
?>