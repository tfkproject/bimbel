<?php
session_start();
if (empty($_SESSION)) {
	header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else{
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
include("head.php");
?>

<body>

    <div id="wrapper">

	<?php include("nav.php"); ?>
	
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$id_siswa = $_GET['id_siswa'];
				$jum_cicil = $_GET['jum_cicil'];
				$id_gelombang = $_GET['id_gelombang'];
				$parameter = '?id_siswa='.$id_siswa.'&jum_cicil='.$jum_cicil.'&id_gelombang='.$id_gelombang;
				
				
				$query = "select * from `siswa` where id_siswa = $id_siswa;";
				$eksekusi = mysqli_query($koneksi, $query);
				while($row = mysqli_fetch_array($eksekusi)){
				?>
					<h1 class="page-header">Rincian Pembayaran <b><?php echo $row['nama'];?></b></h1>
				<?php
				}
				?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tabel Data Siswa
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						
						<!-- /.table-responsive -->
                            <div class="well" id="btn_transaksi">
                                <a class="btn btn-primary btn-lg btn-block" href="add_pembayaran.php<?php echo $parameter;?>"><i class="fa fa-plus"></i>&nbsp;Transaksi</a>
                            </div>
						
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Ansuran ke</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								    $id_gelombang = $_GET['id_gelombang'];
									
									$query = "select * from `pembayaran` where `id_siswa` = $id_siswa";
									$eksekusi = mysqli_query($koneksi, $query);
									while($row = mysqli_fetch_array($eksekusi)){
										$id = $row['id_pembayaran'];
										$parameter = '?id_pembayaran='.$id.'&id_siswa='.$id_siswa.'&jum_cicil='.$jum_cicil.'&id_gelombang='.$id_gelombang;
								?>
                                    <tr class="gradeU">
										<td><?php echo $row['ansuran_ke'];?></td>
										<td>Rp.&nbsp;<?php echo $row['jumlah'];?></td>
										<td><?php echo $row['tanggal'];?></td>
										<td class="center">
											<a href="edit_pembayaran.php<?php echo $parameter;?>"><button type="button" class="btn btn-success btn-xs"><i class="fa fa-pencil fa-fw"></i></button></a>
											<a href="hapus_pembayaran.php<?php echo $parameter;?>" onClick="return confirm('Yakin ingin menghapus data?');">
												<button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash fa-fw"></i></button>
											</a>
										</td>
                                    </tr>
                                <?php
								}
								?>
                                    
                                </tbody>
                            </table>
                            <div class="well">
                                <?php
                                	$query = "select * from `registrasi` inner join `siswa` on `registrasi`.`id_siswa` = `siswa`.`id_siswa` inner join `gelombang` on `registrasi`.`id_gelombang` = `gelombang`.`id_gelombang` where `registrasi`.`id_siswa` = $id_siswa";
									$eksekusi = mysqli_query($koneksi, $query);
									while($row = mysqli_fetch_array($eksekusi)){
										//$by_bimbel = $row['biaya_bimbel'];
										
										$query2= "select sum(jumlah) as total from pembayaran where id_siswa = $id_siswa";
										$eksekusi2 = mysqli_query($koneksi, $query2);
										$row2 = mysqli_fetch_array($eksekusi2)
								?>
                                
                                    Biaya Bimbel:<br>
                                    <b>Rp.&nbsp;<?php echo $by_bim = $row['biaya_bimbel'];?></b><br>
                                    Total Bayar:<br>
                                    <b>Rp.&nbsp;<?php echo $total = $row2['total'];?></b><br>
                                    Sisa:<br>
                                    <b>Rp.&nbsp;<?php echo $sisa = $total - $by_bim;?></b><br>
                                    Keterangan:
                                    <?php
                                    if($sisa < 0){
                                        $query3= " update registrasi set keterangan = 'B' where id_siswa = $id_siswa and id_gelombang = $id_gelombang";
										$eksekusi3 = mysqli_query($koneksi, $query3);
                                        ?>
                                        <font color="red"><b>BELUM LUNAS</b></font>
                                        <?php
                                    }
                                    else{
                                        $query3= " update registrasi set keterangan = 'L' where id_siswa = $id_siswa and id_gelombang = $id_gelombang";
										$eksekusi3 = mysqli_query($koneksi, $query3);
                                        ?>
                                        <script>
											var btn = document.getElementById("btn_transaksi");
											btn.style.display = 'none';
										</script>
                                        <font color="green"><b>LUNAS</b></font>
                                        <?php
                                    }
                                    ?>
                                <?php
								}
								?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php
	include("script.php");
	?>

</body>

</html>
<?php
}
?>