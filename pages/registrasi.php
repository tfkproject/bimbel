<?php
session_start();
if (empty($_SESSION)) {
	header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else{
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
include("head.php");
?>

<body>

    <div id="wrapper">

	<?php include("nav.php"); ?>
	
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Siswa Teregistrasi</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tabel Data Teregistrasi
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						
						<!-- /.table-responsive -->
                            <div class="well">
                                <a class="btn btn-primary btn-lg btn-block" href="pil_siswa.php"><i class="fa fa-plus"></i>&nbsp;Registrasi</a>
                            </div>
						
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Tahun</th>
                                        <th>Gelombang Ke</th>
                                        <th>Biaya Bimbel</th>
                                        <th>Jumlah Cicilan</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
									
									<?php
									$query = "select * from `registrasi` inner join `siswa` on `registrasi`.`id_siswa` = `siswa`.`id_siswa` inner join `gelombang` on `registrasi`.`id_gelombang` =  `gelombang`.`id_gelombang`";
									$eksekusi = mysqli_query($koneksi, $query);
									while($row = mysqli_fetch_array($eksekusi)){
										$id = $row['id_registrasi'];
										$parameter = '?id_registrasi='.$id;
									?>									
										<tr class="gradeU">
											<td><?php echo $row['nama'];?></td>
											<td><?php echo $row['tahun'];?></td>
											<td><?php echo $row['gelombang_ke'];?></td>
											<td class="center">Rp.&nbsp;<?php echo $row['biaya_bimbel'];?></td>
											<td class="center"><?php echo $row['jumlah_cicilan'];?>&nbsp;X</td>
											<td class="center">
											<?php
											$ket = $row['keterangan'];
											if($ket == 'B'){
												?>
												<font color="red">Belum Lunas</font>
												<?php
											}
											if($ket == 'L'){
												?>
												<font color="green">Lunas</font>
												<?php
											}
											?>
											</td>
											<td class="center">
												<a href="edit_registrasi.php<?php echo $parameter;?>"><button type="button" class="btn btn-success btn-xs"><i class="fa fa-pencil fa-fw"></i></button></a>
												<a href="hapus_registrasi.php<?php echo $parameter;?>" onClick="return confirm('Yakin ingin menghapus data?');">
													<button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash fa-fw"></i></button>
												</a>
											</td>
										</tr>
									<?php
									}
									?>
                                </tbody>
                            </table>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
        </div>
        <!-- /#page-wrapper -->
	
    </div>
    <!-- /#wrapper -->

    <?php
	include("script.php");
	?>

</body>

</html>
<?php
}
?>