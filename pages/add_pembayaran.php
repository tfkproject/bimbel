<?php
session_start();
if (empty($_SESSION)) {
	header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else{
	include("koneksi.php");
?>
<!DOCTYPE html>
<html lang="en">

<?php
include("head.php");
?>

<body>

    <div id="wrapper">

	<?php include("nav.php"); ?>
	
	
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pembayaran Uang Bimbel</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Input Data
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
									<?php
									$id_siswa = $_GET['id_siswa'];
									$jum_cicil = $_GET['jum_cicil'];
									$id_gelombang = $_GET['id_gelombang'];
									$parameter = '?id_siswa='.$id_siswa.'&jum_cicil='.$jum_cicil.'&id_gelombang='.$id_gelombang;
									?>
                                    <form role="form" action="add_pembayaran_process.php<?php echo $parameter;?>" method="POST">
                                        <div class="form-group">
											<label>Ansuran</label>
											<div class="form-group input-group">
												<span class="input-group-addon">Ke</span>														
												<input type="number" name="ans" class="form-control">
											</div>
										</div>
                                        <div class="form-group">
											<label>Jumlah</label>
											<div class="form-group input-group">
												<span class="input-group-addon">Rp</span>														
												<input type="number" name="jum" class="form-control">
											</div>
										</div>
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input name="tgl" type="date" class="form-control">
                                        </div>
                                        <button name="submit" value="submit" type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php
	include("script.php");
	?>

</body>

</html>
<?php
}
?>